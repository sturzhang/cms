import java.io.*;
import java.util.*;
import java.nio.file.*;

public class Page {
    public static String pathtomain = "";
    public static String pathtomainhtml = "";
    public static String[] accepted_file_formats = new String [] {"jpg", "jpeg", "png", "pdf", "txt", "gif", "zip", "mp4"};
    private static int SLASH = 47;

    public static String TREE_DOWN = "Subpages:";
    public static String TREE_UP = "Up:";

    
    private boolean leaf = true;
    private boolean root = true;
    private int hierarchy_level = 0;
    private boolean unlinked = false;
    private String lastModified = "";
    private String filename = "";
    private String title = "";
    private String caption = "";
    private String relativepath = "";
    private String htmlpath = "";
    private String htmlpathdir = "";
    private String content = "";
    private LinkedList<Page> subpages = new LinkedList<Page>();
    private LinkedList<String> associated_files = new LinkedList<String>();
    private String[] linksused;
    private Page predecessor;
    private Website website;

    // ### constructors and setter methods ######################################################################

    public Page(String pfilename, String ptitle, String pcaption, Website website){
	relativepath = "";
	this.website = website;
	init(pfilename, ptitle, pcaption);
    }

    public Page(String pfilename, String ptitle, Page ppredecessor, boolean punlinked, String lastMod, Website website){
	this.website = website;
	root = false;
	unlinked = punlinked;
	lastModified = lastMod;
	predecessor = ppredecessor;
	relativepath = predecessor.relativepath() + "/" + pfilename;  // no extensions here, this is a path to a directory!
	hierarchy_level = predecessor.hierarchy_level() + 1;
	init(pfilename, ptitle, ptitle);
    }
    
    private void init(String pfilename, String ptitle, String pcaption){
	filename = pfilename;
	if (ptitle.startsWith(website.settings.UNLINKED_TAG())){
	    title = ptitle.substring(1, ptitle.length());
	} else {
	    title = ptitle;
	}
	caption = pcaption;
	content = Html.change_strange_characters(Fileinteraction.readfile(pathtomain + relativepath + "/" + website.settings.INTERNAL_USE_TAG() + website.settings.CONTENT_FILE_NAME()));	
	linksused = Html.get_all_links(content);
	set_htmlpath();
    }

    private void set_htmlpath(){
	if (root){
	    htmlpath = pathtomainhtml + "/" + filename + website.settings.HTML_EXTENSION();
	} else {
	    if (unlinked){
		htmlpath = pathtomainhtml + delete_unlinked_tag(relativepath) + website.settings.HTML_EXTENSION();
	    } else {
		htmlpath = pathtomainhtml + relativepath + website.settings.HTML_EXTENSION();
	    }
	}
	htmlpathdir = htmlpath;
	for (int i = 1; i <= htmlpath.length(); i++){
	    if (htmlpathdir.charAt(htmlpathdir.length()-1) == SLASH){ 
		break;
	    }
	    htmlpathdir = htmlpathdir.substring(0, htmlpathdir.length()-1);
	}
	htmlpathdir = htmlpathdir.substring(0, htmlpathdir.length()-1);
    }

    private void set_leaf(boolean val){
	leaf = val;
	set_htmlpath();
    }


    // ### getter methods ###################################################################

    public String filename(){ return filename; }

    public int hierarchy_level(){ return hierarchy_level; }

    public String htmlpath(){ return htmlpath; }

    public String relativepath(){ return relativepath; }

    public String title(){ return title; }

    public String caption(){ return caption; }

    public boolean unlinked(){ return unlinked; }

    public Page predecessor(){ return predecessor; }

    public LinkedList<Page> subpages(){ return subpages; }

    // ### to HTML ###########################################################################


    public void produce_html(){
	if ((!leaf) && (!root)){
	    Fileinteraction.createdir(pathtomainhtml + relativepath);
	}
	Fileinteraction.writefile(htmlpath, tohtml());
	
	
	// copy recognized files:
	for (String p : associated_files){
	    Fileinteraction.copyreplacefile(pathtomain + relativepath + "/" + p, htmlpathdir + "/" + p);
	}
	
	// ask your successors to write themselves
	for (Page p : subpages){
	    p.produce_html();
	}
    }

    public String get_links_to_subpages_old(Page donotlinkme, String separator){
	// old version: tilde-separated list
	String subs = "";
	boolean firsttime = true;
	for (Page p : subpages){
	    if (!p.unlinked()){
		if (p != donotlinkme){
		    subs += (firsttime ? "" : separator) + Html.link_to_page(p, this);
		} else {
		    subs += (firsttime ? "" : separator) + Html.unlink_to_page(p);
		}
		firsttime = false;
	    }
	}
	return subs;
    }

    public String get_links_to_subpages(Page donotlinkme, String separator){
	// new version <ul>-list
	String subs = "";
	boolean firsttime = true;
	for (Page p : subpages){
	    if (!p.unlinked()){
		if (p != donotlinkme){
		    subs += (firsttime ? "" : "\n") + Html.list_item(Html.link_to_page(p, this));
		}
		firsttime = false;
	    }
	}
	return subs;
    }

    private String get_links_to_subpages(Page p){
	String res = "";
	for (Page subpage : p.subpages()){
	    res += Html.list_item(Html.link_to_page(subpage, this)) + "\n";
	}
	return res;
    }

    private String get_link_and_links_to_subpages(Page p){
	return get_link_and_links_to_subpages(p, 0);
    }

    private String tabForNaviList(){ return "&emsp;"; }
    
    private String get_link_and_links_to_subpages(Page p, int numberOfTabs){
	String res = "";

	for (int k = 1; k <= numberOfTabs; k++){ res += tabForNaviList(); }

	res += (p == this ? p.caption() : Html.link_to_page(p, this)) + "\n";

	String resList = "";
	res += Html.list(get_links_to_subpages(p));
	
	return res;
    }
    
    private String get_tree_links(){
	// new version adapted to a dropdown side bar

	String linkToMainPage = "";
	if (predecessor != null){
	    linkToMainPage += Html.list(Html.list_item(Html.link_to_page(website.mainpage(), this)));
	    linkToMainPage += Html.par("") + "\n";
	    linkToMainPage += Html.par("") + "\n";
	}
	
	Page subpageOfMainpage = this;
	String pathToMe = Html.list_item(get_link_and_links_to_subpages(this, hierarchy_level()-1));
	if (predecessor != null){
	    Page predecessorInList = predecessor;
	    while (predecessorInList.predecessor() != null){
		pathToMe = Html.list_item(get_link_and_links_to_subpages(predecessorInList, predecessorInList.hierarchy_level()-1))
		    + "\n" + pathToMe;
		predecessorInList = predecessorInList.predecessor();
		subpageOfMainpage = subpageOfMainpage.predecessor();
	    } 
	}

	String listOfSubpagesOfMainpage = "";
	
	for (Page p : website.mainpage().subpages){
	    if ((predecessor != null) && (p == subpageOfMainpage)){
		listOfSubpagesOfMainpage += pathToMe;
	    } else {
		listOfSubpagesOfMainpage += Html.list_item(get_link_and_links_to_subpages(p));
	    }
	}

	String res = "\n<nav>\n" + linkToMainPage + Html.list(listOfSubpagesOfMainpage) + "\n</nav>";

	//if ((!leaf) && (predecessor != null)){
	//    res += Html.par("") + "\n";
	//}
	
	/*if (predecessor != null){
	    res += "back to:" + Html.list(Html.par("<li>" + Html.link_to_page(predecessor, this)));
	    if (predecessor != website.mainpage()){
		res += "\n";
		
	    }
	    }*/
	return res;
    }
    
    private String get_tree_links_simple_sidebar(){
	// version adapted to a simple side bar
	String res = "\n<nav>\n";
	if (!leaf){
	    res += "subpages:" + Html.list(get_links_to_subpages(null, ""));
	}
	if ((!leaf) && (predecessor != null)){
	    res += Html.par("") + "\n";
	}
	if (predecessor != null){
	    res += "back to:" + Html.list(Html.par("<li>" + Html.link_to_page(predecessor, this)));
	    if (predecessor != website.mainpage()){
		res += "\n";
		res += "home:" + Html.list(Html.par("<li>" + Html.link_to_page(website.mainpage(), this)));
	    }
	}
	return res + "</nav>";
    }


    private String get_tree_links_old(){
	// old version with just a list of links at the bottom of the page
	String res = "";
	if (!leaf){
	    res += Html.par("<b>" + TREE_DOWN + "</b> " + get_links_to_subpages(null, " ~ "));
	}
	if (predecessor != null){
	    res += Html.par(TREE_UP + " " + Html.link_to_page(predecessor, this));
	    //res += Html.par("<div align=\"center\">" + predecessor.get_links_to_subpages(this, " ") + "</div>");
	}
return res;
    }

    private String get_timestamp(){
	if (!root){
	    return "<p style=\"position: absolute; top: 0; left: 0; width: 100%; text-align: right;\"><font size=\"1\">Last modified: " + lastModified + "</font></p>";
	} else {
	    return "";
	}
    }
    
    private String tohtml(){
	String head = Html.head(Html.use_css(get_up_directories() + website.settings.CSS()) + Html.title(website.settings.MAINPAGE_TITLE() + " " + website.settings.TITLE_SPACER() + " " + caption));
	String body = "";

	body += "<div id=\"content\">";
	body += "\n<h1>" + caption + "</h1>\n";
	body += content;

	if (predecessor != null){
	    body += "<br><br>";
	    body += Html.list(get_links_to_subpages(null, ""));
	}
	    
	body += get_timestamp();
	body += "</div>\n";
	
	body += "<div id=\"navi\">";
	body += get_tree_links();
	body += "</div>";
	
	
	return Html.html(head + Html.body(body));
    }

    // ### checking links ###########################################################################################

    public int broken_links(){
	int res = 0;
	String link;

	if (linksused.length > 0){
	    for (int i = 0; i < linksused.length; i++){
		if (linksused[i].startsWith("http")){
		    // uncheckable
		} else {
		    if (linksused[i].endsWith(website.settings.HTML_EXTENSION())){
			link = linksused[i].substring(0, linksused[i].length() - website.settings.HTML_EXTENSION().length());
			if (leaf){
			    link = "../" + link;
			}			
		    } else {
			link = linksused[i];
		    }

		    if (!Fileinteraction.exists(pathtomain + relativepath + "/" + link)){
			res += 1;
			System.err.println("ERROR: Broken link " + linksused[i] + " in file " + relativepath);
		    }
		}
	    }
	}


	for (Page p : subpages){
	    res += p.broken_links();
	}
	return res;
    }


    // ### converting the file structure into class structure #######################################################


    private void add_linked_page(Page page){
	if (leaf) { set_leaf(false); }
	subpages.add(page);
	page.add_linked_pages();
    }

    public void add_linked_pages(){
	File here = new File(pathtomain + relativepath);
	File[] listoffiles = here.listFiles();
	Page successor;
	String tmpfilename;
	
	for (int i = 0; i < listoffiles.length; i++){
	    if (listoffiles[i].isDirectory()){
		tmpfilename = listoffiles[i].getName();
		if (!tmpfilename.startsWith(website.settings.INTERNAL_USE_TAG())){
		    add_linked_page(new Page(tmpfilename, 
					     Html.change_strange_characters(tmpfilename), 
					     this, tmpfilename.startsWith(website.settings.UNLINKED_TAG()), 
					     (new Date(listoffiles[i].lastModified())).toString(), website)
				    );
		}
	    }
	    if (listoffiles[i].isFile()){
		if (accepted_file_format(listoffiles[i].getName())){
		    associated_files.add(listoffiles[i].getName());
		}

	    }
	}
    }


    // ### general file and String interaction ########################################################################

    public boolean accepted_file_format(String path){
	boolean res = false;
	path = path.toLowerCase();
	
	for (String f : accepted_file_formats){
	    if ((path.endsWith("." + f)) && (!(path.startsWith(website.settings.INTERNAL_USE_TAG())))){
		res = true;
		break;
	    }
	}
	return res;
    }
 
    private String get_up_directories(){
	return get_up_directories(hierarchy_level - website.settings.HIERARCHY_LEVEL_SHIFT); // TODO there seems a problem witrh linking to css file; added -1
    }

    private String get_up_directories(int n){
	String res = "";
	for (int i = 1; i <= n; i++){
	    res = "../" + res;
	}
	return res;
    }

    public String htmlpath_from(Page from){
	// how to get from the given page to me??

	/* example:
	me:  base/dir1/dir2/me.html
	him: base/dir1/dir3/him.html
	result:     ../dir3/him.html
	*/

	String me = htmlpath;
	String him = from.htmlpath();
	boolean started = false;
	int lastslash = 0;
	String res = "";
	
	for (int i = 1; i <= Math.min(me.length(), him.length()); i++){
	    if (me.regionMatches(0, him, 0, i)){
		if (i == 1){
		    // ok, we are on good track
		    started = true;
		} else {
		    if (!started){
			System.out.println("There is some path problem: Failed to create relative path from " + me + " to " + him);
			break;
		    } else {
			if (him.charAt(i-1) == SLASH){
			    lastslash = i-1;
			}
			// OK we are on our way
		    }
		}
	    } else {
		if (started){
		    // here we got the first index which does not match
		    res = me.substring(lastslash+1, me.length());
		    break;
		}
	    }
	}

	String himdiff = him.substring(lastslash + 1, him.length());
	int nslashs = himdiff.length() - himdiff.replace("/", "").length();
	res = get_up_directories(nslashs) + res;

	/*System.out.println("   me:" + me);
	System.out.println(" from:" + him);
	System.out.println("  res:" + res);*/

	return res;
    }

    private String delete_unlinked_tag(String path){
	return path.replace(website.settings.UNLINKED_TAG(), "");
    }
    

}
