public class Website {

    private static Page mainpage;
    //public static Settings settings = new SettingsUZH();
    public static Settings settings = new SettingsSturzhang();

    // DON'T FORGET TO RECOMPILE !!!!!
    // ###################################

    public static void main(String[] args){
	if (args.length != 1){
	    System.out.println("Please give the directory, where to find the content of the main page!");
	} else {
	    Website website = new Website(args[0]);
	}
    }

    public Website(String pathtomainpage){
	if (pathtomainpage.trim().endsWith("/")){
	    pathtomainpage = pathtomainpage.substring(0, pathtomainpage.length()-1);
	}
	Page.pathtomain = pathtomainpage;
	if (settings.putHtmlIntoDirectoryWithSuffix()){
	    Page.pathtomainhtml = pathtomainpage + settings.HTML_SUFFIX();
	} else {
	    Page.pathtomainhtml = settings.PATH_TO_MAIN_HTML();
	}

	mainpage = new Page(settings.MAINPAGE_FILENAME(), settings.MAINPAGE_TITLE(), settings.MAINPAGE_CAPTION(), this);
	mainpage.add_linked_pages();
	
	if (mainpage.broken_links() == 0){
	    Fileinteraction.deleteandcreatedir(Page.pathtomainhtml);
	    
	    Fileinteraction.copyreplacefile(settings.PATH_TO_CSS() + settings.CSS(), Page.pathtomainhtml + "/" + settings.CSS());
	    mainpage.produce_html();
	}
    }

    public Page mainpage(){
	return mainpage;
    }
    
}
