public class SettingsSturzhang extends Settings {

    public SettingsSturzhang(){
	MAINPAGE_FILENAME = "index";  // no extension here

	putHtmlIntoDirectoryWithSuffix = true;
	HTML_SUFFIX = "_html";
	PATH_TO_MAIN_HTML = "sturzhang_html";
        
	CONTENT_FILE_NAME = "inhalt.txt"; // extension please
	HTML_EXTENSION = ".html";
	MAINPAGE_TITLE = "sturzhang.de";
	MAINPAGE_CAPTION = "sturzhang.de";
	TITLE_SPACER = "~";
	INTERNAL_USE_TAG = "_";
	UNLINKED_TAG = "*";
	PATH_TO_CSS = "";  // has to end with a slash!
	CSS = "style.css";  // extension please

	HIERARCHY_LEVEL_SHIFT = 0;
    }



}
