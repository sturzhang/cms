import java.util.*;

public class Html {

    public static String body(String content){
	return "\n<body>\n" + content + "\n</body>";
    }
    
    public static String head(String content){
	return "\n<head>\n" + content + "\n</head>";
    }

    public static String title(String content){
	return "\n<title>" + content + "</title>";
    }

    public static String par(String content){
	return "\n<p>" + content + "</p>";
    }

    public static String list(String content){
	return "\n<ul>\n" + content + "</ul>";
    }

    public static String list_item(String content){
	return "<li>" + content + "</li>\n";
    }

    public static String html(String content){
	return "<!DOCTYPE html>\n\n<html>\n" + content + "</html>";
    }

    public static String use_css(String path){
	return "<link href=\"" + path + "\" rel=\"stylesheet\"  type=\"text/css\">";
    }

    public static String link(String path, String content){
	return "<a href=\"" + path + "\">" + content + "</a>";
    }

    public static String link_to_page(Page to, Page from){
	return link(Html.change_strange_characters(to.htmlpath_from(from)), to.caption());
    }

    public static String unlink_to_page(Page page){
	return page.title();
    }

    public static String center(String content){
	return "<center>"+content+"</center>";
    }

    public static String[] get_all_links(String s){
	int[] starters, enders;
	String[] res;
	String tmp2;
	String tmp;
	int startoflink, secondquote;

	s = s.replace(" ", "");

	starters = get_positions_of_symbol("<a", s);
	enders = get_positions_of_symbol("</a>", s);
	if (starters.length != enders.length){
	    System.err.println("Not the same number of <a and </a> symbols in " + s + "!");
	}
	
	res = new String[starters.length];
	for (int i = 0; i < starters.length; i++){
	    tmp2 = s.substring(starters[i], enders[i]);
	    tmp = s.substring(starters[i]+2, starters[i] + tmp2.indexOf(">"));
	    // tmp is the content of the first pair of angle brackets
	    startoflink = tmp.indexOf("href=");
	    res[i] = tmp.substring(startoflink+5, tmp.length());
	    // we assume there is nothing behind the link!
	    if (res[i].startsWith("\"") && res[i].endsWith("\"")){
		res[i] = res[i].substring(1, res[i].length()-1);
	    }
	}
	return res;
    }

    private static int[] get_positions_of_symbol(String symbol, String s){
	LinkedList<Integer> pos = new LinkedList<Integer>();
	int foundat = 0;
	do {
	    foundat = s.indexOf(symbol, foundat);
	    if (foundat > -1){
		pos.add(foundat);
	    }
	    foundat++;
	} while(foundat != 0);
	
	int[] res = new int[pos.size()];
	int counter = 0;
	for (Integer i : pos){
	    res[counter] = i;
	    counter++;
	}
	return res;
    }

    public static String change_strange_characters(String text){
	String newtext = "";
	char c;
	
	//	String st = "á";
	//      System.out.println((int) st.charAt(0));

	for (int i = 0; i < text.length(); i++){
	    c = text.charAt(i);
	    if ((int) c == 252){
		newtext += "&uuml;";
	    } else {
		if ((int) c == 228){
		    newtext += "&auml;";
		} else {
		    if ((int) c == 246){
			newtext += "&ouml;";
		    } else {
			if ((int) c == 223){
			    newtext += "&szlig;";
			} else {
			    if ((int) c == 220){
				newtext += "&Uuml;";
			    } else {
				if ((int) c == 224){
				    newtext += "&agrave;";
				} else {
				    if ((int) c == 225){
					newtext += "&aacute;";
				    } else {
					if ((int) c == 232){
					    newtext += "&egrave;";
					} else {
					    if ((int) c == 233){
						newtext += "&eacute;";
					    } else {
						if ((int) c == 196){
						    newtext += "&Auml;";
						} else {
						    if ((int) c == 214){
							newtext += "&Ouml;";
						    } else {
							newtext += c;
						    }}}}}}}}}}}}
	
	return newtext;
    }
    
}
