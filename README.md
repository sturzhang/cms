# cms

Creates a website out of a directory <dir>.


To run:

javac *.java; java Website <dir>


Licence: CC BY-NC-SA (<a href="https://creativecommons.org/licenses/by-nc-sa/3.0/de/">info</a>)
