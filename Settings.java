public abstract class Settings {


    protected String MAINPAGE_FILENAME;

    protected boolean putHtmlIntoDirectoryWithSuffix;
    protected String HTML_SUFFIX;
    protected String PATH_TO_MAIN_HTML;
        
    protected String CONTENT_FILE_NAME;
    protected String HTML_EXTENSION;
    protected String MAINPAGE_TITLE;
    protected String MAINPAGE_CAPTION;
    protected String TITLE_SPACER;
    protected String INTERNAL_USE_TAG;
    protected String UNLINKED_TAG;
    protected String PATH_TO_CSS;
    protected String CSS;
    protected int HIERARCHY_LEVEL_SHIFT;
    
    public String MAINPAGE_FILENAME(){ return MAINPAGE_FILENAME; }

    public boolean putHtmlIntoDirectoryWithSuffix(){ return putHtmlIntoDirectoryWithSuffix; }
    public String HTML_SUFFIX(){ return HTML_SUFFIX; }
    public String PATH_TO_MAIN_HTML(){ return PATH_TO_MAIN_HTML; }
        
    public String CONTENT_FILE_NAME(){ return CONTENT_FILE_NAME; }
    public String HTML_EXTENSION(){    return HTML_EXTENSION; }
    public String MAINPAGE_TITLE(){    return MAINPAGE_TITLE; }
    public String MAINPAGE_CAPTION(){  return MAINPAGE_CAPTION; }
    public String TITLE_SPACER(){      return TITLE_SPACER; }
    public String INTERNAL_USE_TAG(){  return INTERNAL_USE_TAG; }
    public String UNLINKED_TAG(){      return UNLINKED_TAG; } 
    public String PATH_TO_CSS(){       return PATH_TO_CSS; }
    public String CSS(){               return CSS; }

    public int 	HIERARCHY_LEVEL_SHIFT(){ return HIERARCHY_LEVEL_SHIFT; }
}
